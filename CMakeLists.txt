cmake_minimum_required(VERSION 3.4)
project(ThomasWasLate)
set(CMAKE_CXX_STANDARD 14)

# WARNING - THIS LINE SHOULD BE REMOVED FOR RELEASE!
# set(CMAKE_BUILD_TYPE Debug)

# Debug mode
if(${CMAKE_BUILD_TYPE} STREQUAL Debug)
    message("-- The project will be compiled in DEBUG mode")
    add_definitions(-DDEBUG)
endif()

# Release mode and linux
if(${CMAKE_BUILD_TYPE} STREQUAL Release AND UNIX AND NOT APPLE)
    set(CMAKE_C_FLAGS_RELEASE "${CMAKE_C_FLAGS_RELEASE} -s")
    set(CMAKE_CXX_FLAGS_RELEASE "${CMAKE_CXX_FLAGS_RELEASE} -s")
endif()

# Add SFML 2.3
set(CMAKE_MODULE_PATH "${CMAKE_CURRENT_SOURCE_DIR}/Modules" ${CMAKE_MODULE_PATH})
find_package(SFML 2.3 COMPONENTS system window graphics audio REQUIRED)
include_directories(${SFML_INCLUDE_DIR})

find_package(OpenMP REQUIRED)


# Compile final executable
set(SOURCE_FILES src/TextureHolder.cpp src/Engine.cpp src/Input.cpp src/Update.cpp src/Draw.cpp src/Main.cpp src/PlayableCharacter.cpp src/Thomas.cpp src/Bob.cpp src/LevelManager.cpp src/LoadLevel.cpp src/DetectCollisions.cpp src/SoundManager.cpp src/PopulateEmitters.cpp src/HUD.cpp src/Particle.cpp src/ParticleSystem.cpp)

set(HEADER_FILES src/TextureHolder.hpp src/Engine.hpp src/PlayableCharacter.hpp src/Thomas.hpp src/Bob.hpp src/LevelManager.hpp src/SoundManager.hpp src/HUD.hpp src/Particle.hpp src/ParticleSystem.hpp)

set(SHADER_FILES shaders/rippleShader.frag shaders/vertShader.vert)

add_executable(ThomasWasLate ${SOURCE_FILES} ${HEADER_FILES})

# Link
target_link_libraries(ThomasWasLate ${SFML_LIBRARIES} ${OpenMP_CXX_FLAGS})
