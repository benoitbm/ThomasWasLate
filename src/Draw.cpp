//
// Created by bboyer02 on 17/11/17.
//

#include "Engine.hpp"

void Engine::draw() {
    m_Window.clear(Color::White);

    m_RippleShader.setParameter("uTime", m_GameTimeTotal.asSeconds());

    if (!m_SplitScreen)
    {
        m_Window.setView(m_MainBGView);
        m_Window.draw(m_BackgroundSprite, &m_RippleShader);


        m_Window.setView(m_MainView);
        m_Window.draw(m_VALevel, &m_TextureTiles);

        m_Window.draw(m_Thomas.getSprite());
        m_Window.draw(m_Bob.getSprite());

        if (m_PS.running())
            m_Window.draw(m_PS);
    }
    else
    { //Split screen mode activated
        //Left view
        m_Window.setView(m_LeftBGView);
        m_Window.draw(m_BackgroundSprite, &m_RippleShader);;
        m_Window.setView(m_LeftView);

        m_Window.draw(m_VALevel, &m_TextureTiles);

        m_Window.draw(m_Bob.getSprite());
        m_Window.draw(m_Thomas.getSprite());

        if (m_PS.running())
            m_Window.draw(m_PS);

        //Right view
        m_Window.setView(m_RightBGView);
        m_Window.draw(m_BackgroundSprite, &m_RippleShader);
        m_Window.setView(m_RightView);

        m_Window.draw(m_VALevel, &m_TextureTiles);

        m_Window.draw(m_Thomas.getSprite());
        m_Window.draw(m_Bob.getSprite());

        if (m_PS.running())
            m_Window.draw(m_PS);
    }

    m_Window.setView(m_HUDView);
    m_Window.draw(m_HUD.getLevel());
    m_Window.draw(m_HUD.getTime());
    if (!m_Playing)
        m_Window.draw(m_HUD.getMessage());

    m_Window.display();
}