//
// Created by benoit on 20/11/17.
//

#include "Thomas.hpp"
#include "TextureHolder.hpp"

Thomas::Thomas() {
    m_Sprite = Sprite(TextureHolder::GetTexture(DIRECTORY+"thomas.png"));
    m_JumpDuration = .45;
}

bool Thomas::handleInput() {
    m_JustJumped = false;

    if (Keyboard::isKeyPressed(Keyboard::Z))
    {
        if (!m_IsJumping && !m_IsFalling)
        {
            m_IsJumping = true;
            m_TimeThisJump = 0;
            m_JustJumped = true;
        }
    }
    else
    {
        m_IsJumping = false;
        m_IsFalling = true;
    }

    m_LeftPressed = Keyboard::isKeyPressed(Keyboard::Q);
    m_RightPressed = Keyboard::isKeyPressed(Keyboard::D);

    return m_JustJumped;
}