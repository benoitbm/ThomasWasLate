//
// Created by bboyer02 on 01/12/17.
//

#include "ParticleSystem.hpp"

using namespace sf;
using namespace std;

void ParticleSystem::init(int count) {
    m_Vertices.setPrimitiveType(Points);
    m_Vertices.resize(count);

    for (int i = 0; i < count; ++i)
    {
        srand(time(0) + i);
        float angle = (rand() % 360) * M_PI / 180.0f;
        float speed = (rand() % 600) + 600;

        Vector2f direction(cos(angle) * speed, sin(angle) * speed);
        m_Particles.push_back(Particle(direction));
    }
}

void ParticleSystem::update(float dt) {
    m_Duration -= dt;
    vector<Particle>::iterator i;
    int currentVertex = 0;

    for (i = m_Particles.begin(); i != m_Particles.end(); ++i)
    {
        (*i).update(dt);
        m_Vertices[currentVertex++].position = (*i).getPosition();
    }

    if (m_Duration < 0)
        m_IsRunning = false;
}

void ParticleSystem::emitParticules(Vector2f position) {
    m_IsRunning = true;
    m_Duration = 2;

    vector<Particle>::iterator i;
    int currentVertex = 0;

    for (i = m_Particles.begin(); i != m_Particles.end(); ++i) {
        m_Vertices[currentVertex++].color = Color::Yellow;
        (*i).setPosition(position);
    }
}

void ParticleSystem::draw(RenderTarget &target, RenderStates states) const {
    target.draw(m_Vertices, states);
}