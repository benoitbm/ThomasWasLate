//
// Created by benoit on 26/10/17.
//

#include <assert.h>

#include "TextureHolder.hpp"

TextureHolder* TextureHolder::m_s_Instance = nullptr;

TextureHolder::TextureHolder() {
    assert(m_s_Instance == nullptr);
    m_s_Instance = this;
}

Texture& TextureHolder::GetTexture(string const &filename) {
    auto& m =  m_s_Instance->m_Textures;

    auto keyValuePair = m.find(filename);

    if (keyValuePair != m.end())
    {
        return keyValuePair->second;
    }
    else
    {
        auto& texture = m[filename];
        texture.loadFromFile(filename);

        return texture;
    }
}