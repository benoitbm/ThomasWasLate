//
// Created by bboyer02 on 01/12/17.
//

#ifndef THOMASWASLATE_SOUNDMANAGER_HPP
#define THOMASWASLATE_SOUNDMANAGER_HPP

#pragma once

#include <SFML/Audio.hpp>

using namespace sf;

const std::string SND_DIRECTORY = "../sound/";

class SoundManager {
private:
    SoundBuffer m_FireBuffer,
                m_FallInFireBuffer,
                m_FallInWaterBuffer,
                m_JumpBuffer,
                m_ReachGoalBuffer;

    Sound   m_Fire1Sound, m_Fire2Sound, m_Fire3Sound,
            m_FallInFireSound, m_FallInWaterSound,
            m_JumpSound, m_ReachGoalSound;

    int m_NextSound = 1;

public:
    SoundManager();

    void playFire(Vector2f emitterLocation, Vector2f listenerLocation);

    void playFallInFire();
    void playFallInWater();
    void playJump();
    void playReachGoal();
};


#endif //THOMASWASLATE_SOUNDMANAGER_HPP
