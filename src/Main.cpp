//
// Created by bboyer02 on 17/11/17.
//

#include "Engine.hpp"

int main()
{
    Engine engine;

    engine.run();

    return 0;
}