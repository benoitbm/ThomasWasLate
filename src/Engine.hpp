//
// Created by bboyer02 on 17/11/17.
//

#ifndef THOMASWASLATE_ENGINE_HPP
#define THOMASWASLATE_ENGINE_HPP

#pragma once
#include <SFML/Graphics.hpp>
#include "TextureHolder.hpp"
#include "Thomas.hpp"
#include "Bob.hpp"
#include "LevelManager.hpp"
#include "SoundManager.hpp"
#include "HUD.hpp"
#include "ParticleSystem.hpp"

using namespace sf;

class Engine {
private:
    Thomas m_Thomas;
    Bob m_Bob;

    LevelManager m_LM;

    SoundManager m_SM;

    HUD m_HUD;
    int m_FramesSinceLastHUDUpdate = 0, m_TargetFramesPerHUDUpdate = 100;

    ParticleSystem m_PS;

    const int TILE_SIZE = 50;

    const int GRAVITY = 300;

    RenderWindow m_Window;

    View m_MainView, m_LeftView, m_RightView;
    View m_MainBGView, m_LeftBGView, m_RightBGView;
    View m_HUDView;

    Sprite m_BackgroundSprite;
    Texture m_BackgroundTexture;

    Shader m_RippleShader;

    bool m_Playing = false;

    bool m_Character1 = true; //Focus on P1 ? (or P2 if false)

    bool m_SplitScreen = false;

    float m_TimeRemaining = 10;
    Time m_GameTimeTotal;

    bool m_NewLevelRequired = true;

    VertexArray m_VALevel;
    int** m_ArrayLevel = nullptr;
    Texture m_TextureTiles;

    void input();
    void update(float dtAsSeconds);
    void draw();

    void loadLevel();

    bool detectCollisions(PlayableCharacter& character);

    vector <Vector2f> m_FireEmitters;
    void populateEmitters(vector <Vector2f>& vSoundEmitters, int** arrayLevel);


public:
    Engine();

    void run();

};


#endif //THOMASWASLATE_ENGINE_HPP
