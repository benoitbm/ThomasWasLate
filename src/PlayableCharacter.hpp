//
// Created by bboyer02 on 17/11/17.
//

#ifndef THOMASWASLATE_PLAYABLECHARACTER_HPP
#define THOMASWASLATE_PLAYABLECHARACTER_HPP

#pragma once

#include <SFML/Graphics.hpp>

using namespace sf;

class PlayableCharacter {
protected:
    Sprite m_Sprite;

    float m_JumpDuration;

    bool m_IsJumping, m_IsFalling;

    bool m_LeftPressed, m_RightPressed;

    float m_TimeThisJump;

    bool m_JustJumped = false;

private:
    float m_Gravity;

    float m_Speed = 400;

    Vector2f m_Position;

    FloatRect m_Feet, m_Head, m_Right, m_Left;

    Texture m_Texture;

public:
    void Spawn(Vector2f startPos, float gravity);

    bool virtual handleInput() = 0;

    inline FloatRect getPosition() { return  m_Sprite.getGlobalBounds();};

    inline FloatRect getFeet() {return m_Feet;};
    inline FloatRect getHead() {return m_Head;};
    inline FloatRect getRight() {return m_Right;};
    inline FloatRect getLeft() {return m_Left;};

    inline Sprite getSprite() { return m_Sprite;};

    void stopFalling(float position);
    void stopRight(float position);
    void stopLeft(float position);
    void stopJump();

    inline Vector2f getCenter() { return Vector2f(m_Position.x + m_Sprite.getGlobalBounds().width / 2, m_Position.y + m_Sprite.getGlobalBounds().height / 2);};

    void update(float elapsedTime);
};


#endif //THOMASWASLATE_PLAYABLECHARACTER_HPP
