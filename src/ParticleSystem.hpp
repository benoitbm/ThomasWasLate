//
// Created by bboyer02 on 01/12/17.
//

#ifndef THOMASWASLATE_PARTICLESYSTEM_HPP
#define THOMASWASLATE_PARTICLESYSTEM_HPP

#pragma once

#include <SFML/Graphics.hpp>
#include "Particle.hpp"

using namespace sf;
using namespace std;

class ParticleSystem : public Drawable {
    vector<Particle> m_Particles;
    VertexArray m_Vertices;
    float m_Duration;
    bool m_IsRunning = false;

public:
    virtual void draw(RenderTarget& target, RenderStates states) const;
    void init (int count);

    void emitParticules(Vector2f position);
    void update(float elapsed);

    inline bool running() {return m_IsRunning;};
};


#endif //THOMASWASLATE_PARTICLESYSTEM_HPP
