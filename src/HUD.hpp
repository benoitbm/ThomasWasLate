//
// Created by bboyer02 on 01/12/17.
//

#ifndef THOMASWASLATE_HUD_HPP
#define THOMASWASLATE_HUD_HPP

#pragma once

#include <SFML/Graphics.hpp>

using namespace sf;

class HUD {
private:
    Font m_Font;
    Text m_StartText, m_TimeText, m_LevelText;

public:
    HUD();

    inline Text getMessage() { return m_StartText;};
    inline Text getLevel() { return m_LevelText;};
    inline Text getTime() { return m_TimeText;};

    inline void setLevel(const String& text) {m_LevelText.setString(text);};
    inline void setTime(const String& text) {m_TimeText.setString(text);};
};


#endif //THOMASWASLATE_HUD_HPP
