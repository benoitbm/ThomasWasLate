//
// Created by bboyer02 on 22/11/17.
//

#include <SFML/Graphics.hpp>
#include <SFML/Audio.hpp>
#include <sstream>
#include <fstream>

using namespace std;
using namespace sf;

#include "LevelManager.hpp"
#include "TextureHolder.hpp"

int** LevelManager::nextLevel(VertexArray &rVaLevel) {
    m_LevelSize = Vector2i(0,0);

    ++m_CurrentLevel;

    if (m_CurrentLevel > NUM_LEVELS)
    {
        m_CurrentLevel = 1;
        m_TimeModifier -= .1;
    }

    string levelToLoad;

    switch (m_CurrentLevel)
    {
        case 1:
            levelToLoad = LEVEL_DIRECTORY + "level1.txt";
            m_StartPosition = Vector2f(100, 100);
            m_BaseTimeLimit = 30;
            break;

        case 2:
            levelToLoad = LEVEL_DIRECTORY + "level2.txt";
            m_StartPosition = Vector2f(100, 3600);
            m_BaseTimeLimit = 100;
            break;

        case 3:
            levelToLoad = LEVEL_DIRECTORY + "level3.txt";
            m_StartPosition = Vector2f(1250, 0);
            m_BaseTimeLimit = 30;
            break;

        case 4:
            levelToLoad = LEVEL_DIRECTORY + "level4.txt";
            m_StartPosition = Vector2f(50, 200);
            m_BaseTimeLimit = 50;
            break;
    } //end switch(m_CurrentLevel)

    ifstream inputFile(levelToLoad);
    string s;

    while (getline(inputFile, s))
        ++m_LevelSize.y;

    m_LevelSize.x = s.length();

    inputFile.clear();
    inputFile.seekg(0, ios::beg); //Go back to start of file

    //Creating the array
    int** arrayLevel = new int*[m_LevelSize.y];
    for (int i = 0; i < m_LevelSize.y; ++i)
    {
        arrayLevel[i] = new int[m_LevelSize.x];
    }

    string row;
    int y = 0;

    //Filling the array
    while (inputFile >> row)
    {
        for (int x = 0; x < row.length(); ++x)
        {
            const char val = row[x];
            arrayLevel[y][x] = atoi(&val);
        }

        ++y;
    }

    inputFile.close();

    rVaLevel.setPrimitiveType(Quads);

    rVaLevel.resize(m_LevelSize.x * m_LevelSize.y * VERTS_IN_QUAD);

    int currentVertex = 0;

    for (int x = 0; x < m_LevelSize.x; ++x)
    {
        for (int y = 0; y < m_LevelSize.y; ++y)
        {
            rVaLevel[currentVertex + 0].position = Vector2f((x + 0) * TILE_SIZE, (y + 0) * TILE_SIZE);
            rVaLevel[currentVertex + 1].position = Vector2f((x + 1) * TILE_SIZE, (y + 0) * TILE_SIZE);
            rVaLevel[currentVertex + 2].position = Vector2f((x + 1) * TILE_SIZE, (y + 1) * TILE_SIZE);
            rVaLevel[currentVertex + 3].position = Vector2f((x + 0) * TILE_SIZE, (y + 1) * TILE_SIZE);

            int verticalOffset = arrayLevel[y][x] * TILE_SIZE;

            rVaLevel[currentVertex + 0].texCoords = Vector2f(0,         0 + verticalOffset);
            rVaLevel[currentVertex + 1].texCoords = Vector2f(TILE_SIZE, 0 + verticalOffset);
            rVaLevel[currentVertex + 2].texCoords = Vector2f(TILE_SIZE, TILE_SIZE + verticalOffset);
            rVaLevel[currentVertex + 3].texCoords = Vector2f(0,         TILE_SIZE + verticalOffset);

            currentVertex += VERTS_IN_QUAD;
        }
    }

    return arrayLevel;
}