//
// Created by bboyer02 on 17/11/17.
//

#include <sstream>
#include "Engine.hpp"

void Engine::update(float dtAsSeconds) {

    if (m_NewLevelRequired)
    {
        //Spawn players
        //m_Thomas.Spawn(Vector2f(0,0), GRAVITY);
        //m_Bob.Spawn(Vector2f(100,0), GRAVITY);

        //m_TimeRemaining = 10;
        //m_NewLevelRequired = false;

        loadLevel();
    }

    if (m_Playing)
    {
        m_Thomas.update(dtAsSeconds);
        m_Bob.update(dtAsSeconds);

        if (detectCollisions(m_Thomas) && detectCollisions(m_Bob))
        {
            m_NewLevelRequired = true;

            //Play sound
            m_SM.playReachGoal();
        }
        else
        {
            detectCollisions(m_Bob); //Detect bob second time to check if on head of Thomas
        }

        if (m_Bob.getFeet().intersects(m_Thomas.getHead()))
            m_Bob.stopFalling(m_Thomas.getHead().top);
        else if (m_Thomas.getFeet().intersects(m_Bob.getHead()))
            m_Thomas.stopFalling(m_Bob.getHead().top);

        m_TimeRemaining -= dtAsSeconds;

        if (m_TimeRemaining <= 0)
        {
            m_NewLevelRequired = true;
        }

    } //End if playing

    //Check if fire sound needs to be played
    vector<Vector2f>::iterator it;

    for (it = m_FireEmitters.begin(); it != m_FireEmitters.end(); ++it)
    {
        float posX = (*it).x;
        float posY = (*it).y;

        FloatRect localRect(posX - 250, posY - 250, 500, 500);

        if (m_Thomas.getPosition().intersects(localRect))
        {
            m_SM.playFire(Vector2f(posX, posY), m_Thomas.getCenter());
        }
    }

    if (m_SplitScreen)
    {
        m_LeftView.setCenter(m_Thomas.getCenter());
        m_RightView.setCenter(m_Bob.getCenter());
    }
    else
    {
        if (m_Character1)
            m_MainView.setCenter(m_Thomas.getCenter());
        else
            m_MainView.setCenter(m_Bob.getCenter());
    }

    //HUD Update
    ++m_FramesSinceLastHUDUpdate;

    if (m_FramesSinceLastHUDUpdate >= m_TargetFramesPerHUDUpdate)
    {
        stringstream ssTime, ssLevel;

        ssTime << (int)m_TimeRemaining;
        m_HUD.setTime(ssTime.str());

        ssLevel << "Level : " << m_LM.getCurrentLevel();
        m_HUD.setLevel(ssLevel.str());

        m_FramesSinceLastHUDUpdate = 0;
    }

    //Particles update
    if (m_PS.running())
        m_PS.update(dtAsSeconds);

}