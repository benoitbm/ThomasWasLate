//
// Created by bboyer02 on 17/11/17.
//

#include "Engine.hpp"

using namespace sf;

Engine::Engine() {
    if(! Shader::isAvailable())
        m_Window.close();
    else
        m_RippleShader.loadFromFile("../shaders/vertShader.vert", "../shaders/rippleShader.frag");


    Vector2f resolution;
    resolution.x = VideoMode::getDesktopMode().width;
    resolution.y = VideoMode::getDesktopMode().height;

    m_Window.create(VideoMode(resolution.x, resolution.y), "Thomas Was Late", Style::Fullscreen);

    m_MainView.setSize(resolution);
    m_HUDView.reset(FloatRect(0, 0, resolution.x, resolution.y));

    m_LeftView.setViewport(FloatRect(0.001f, 0.001f, 0.498f, 0.998f));
    m_RightView.setViewport(FloatRect(0.5f, 0.001f, 0.498f, 0.998f));

    m_LeftBGView.setViewport(FloatRect(0.001f, 0.001f, 0.498f, 0.998f));
    m_RightBGView.setViewport(FloatRect(0.5f, 0.001f, 0.498f, 0.998f));

    m_BackgroundTexture = TextureHolder::GetTexture(DIRECTORY+"background.png");
    m_BackgroundSprite.setTexture(m_BackgroundTexture);

    m_TextureTiles = TextureHolder::GetTexture(DIRECTORY+"tiles_sheet.png");

    m_PS.init(1000);
}

void Engine::run() {
    Clock clock;

    while (m_Window.isOpen())
    {
        Time dt = clock.restart();

        m_GameTimeTotal += dt;
        float dtAsSeconds = dt.asSeconds();

        input();
        update(dtAsSeconds);
        draw();
    }
}
