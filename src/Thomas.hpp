//
// Created by benoit on 20/11/17.
//

#ifndef THOMASWASLATE_THOMAS_HPP
#define THOMASWASLATE_THOMAS_HPP

#pragma once

#include "PlayableCharacter.hpp"

class Thomas : public PlayableCharacter {

public:
    Thomas();

    bool virtual handleInput();

};


#endif //THOMASWASLATE_THOMAS_HPP
