//
// Created by benoit on 20/11/17.
//

#ifndef THOMASWASLATE_BOB_HPP
#define THOMASWASLATE_BOB_HPP

#pragma once

#include "PlayableCharacter.hpp"

class Bob : public PlayableCharacter{
public:
    Bob();

    bool virtual handleInput();
};


#endif //THOMASWASLATE_BOB_HPP
