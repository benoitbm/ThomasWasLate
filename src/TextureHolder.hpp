//
// Created by benoit on 26/10/17.
//

#pragma once
#ifndef ZOMBIEARENA_TEXTUREHOLDER_HPP
#define ZOMBIEARENA_TEXTUREHOLDER_HPP

#include <SFML/Graphics.hpp>
#include <map>

using namespace std;
using namespace sf;

const static std::string DIRECTORY = "../graphics/";

class TextureHolder {
    private:
        map<string, Texture> m_Textures;

        static TextureHolder* m_s_Instance;

    public:
        TextureHolder();
        static Texture& GetTexture(string const& filename);
};


#endif //ZOMBIEARENA_TEXTUREHOLDER_HPP
