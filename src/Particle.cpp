//
// Created by bboyer02 on 01/12/17.
//

#include "Particle.hpp"

Particle::Particle(const Vector2f &direction) : m_Velocity(direction) {

}

void Particle::update(const float &dt) {
    m_Position += m_Velocity * dt;
}

void Particle::setPosition(const Vector2f &position) {
    m_Position = position;
}