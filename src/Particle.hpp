//
// Created by bboyer02 on 01/12/17.
//

#ifndef THOMASWASLATE_PARTICLE_HPP
#define THOMASWASLATE_PARTICLE_HPP

#pragma once

#include <SFML/Graphics.hpp>

using namespace sf;

class Particle {
private:
    Vector2f m_Position, m_Velocity;
    inline Particle(){throw std::exception();};

public:
    Particle(const Vector2f& direction);

    void update(const float& dt);

    void setPosition(const Vector2f& position);

    inline Vector2f getPosition() { return m_Position;};
};


#endif //THOMASWASLATE_PARTICLE_HPP
