//
// Created by bboyer02 on 22/11/17.
//

#ifndef THOMASWASLATE_LEVELMANAGER_HPP
#define THOMASWASLATE_LEVELMANAGER_HPP

#pragma once

#include <SFML/Graphics.hpp>
using namespace std;
using namespace sf;

class LevelManager {
private:
    Vector2i m_LevelSize;
    Vector2f m_StartPosition;

    float m_TimeModifier = 1;
    float m_BaseTimeLimit = 0;
    int m_CurrentLevel = 0;
    const int NUM_LEVELS = 4;

    const string LEVEL_DIRECTORY = "../levels/";

public:
    const int TILE_SIZE = 50;
    const int VERTS_IN_QUAD = 4;

    inline float getTimeLimit() { return m_BaseTimeLimit * m_TimeModifier;};

    inline Vector2f getStartPosition() { return m_StartPosition;};

    int** nextLevel(VertexArray& rVaLevel);

    inline Vector2i getLevelSize() { return m_LevelSize;};

    inline int getCurrentLevel() { return m_CurrentLevel;};
};


#endif //THOMASWASLATE_LEVELMANAGER_HPP
